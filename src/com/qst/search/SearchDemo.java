package com.qst.search;

/**
 * @author 刘汉平
 * @date 2019/9/4 19:32
 * 线性查找
 * 效率低，比如要查找1，需要遍历整个数组
 */
public class SearchDemo {
    public static void main(String[] args) {
        //目标数组
        int [] arr = new int[]{8,3,5,2,6,7,0,1};
        //目标元素
        int target = 5;
        //元素在数组中的下标，初始值为-1
        int index = -1;
        //遍历数组中的元素，和你要查找的元素，如果一致就把下标赋值给index
        for (int i=0;i<arr.length;i++){
            if (arr[i]==target){
                index = i;
                break;
            }
        }
        System.out.println("index:"+index);
    }
}
