package com.qst.search;

/**
 * @author 刘汉平
 * @date 2019/9/4 19:53
 * 二分查找，把数组一分为2和你要查找的元素进行对比
 * 提高了查找的效率，但是使用有局限性，要求你的数组是有序的
 */
public class Search2Demo {
    public static void main(String[] args) {
        //目标数组
        int[] arr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        //目标元素
        int target = 3;
        //定义一个变量，存放目标元素的下标，初始值设为-1
        int index = -1;
        //记录初始的位置
        int begin = 0;
        //记录最后的位置
        int end = arr.length - 1;
        //记录中间的位置
        int mind = (begin + end) / 2;
        //死循环，如果查找到目标元素，就跳出循环，如果没有就一直循环下去
        while (true) {
            if (arr[mind] == target) {
                index = mind;
                break;
                //如果中间的元素和目标元素不相等
            } else {
                //如果中间的元素大于你要查找的目标元素
                if (arr[mind] > target) {
                    //你要查找的元素在目标元素的左边，调整最后位置，为中间元素减1
                    end = mind - 1;
                } else {
                    //你要查找的元素在目标元素的邮编，则调整开始的位置，为中间元素加1
                    begin = mind + 1;
                }
                mind = (begin + end) / 2;

            }
        }
        System.out.println("index:" + index);
    }
}
