package com.qst.sort;

import java.util.Arrays;

/**
 * @author 刘汉平
 * @date 2019/9/7 21:43
 * 快速排序
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] nums = {3, 4, 2, 9, 1, 5, 7};
        quickSort(nums, 0, nums.length - 1);
        System.out.println(Arrays.toString(nums));
    }

    /**
     * @param nums  传入的数组
     * @param start 记录开始元素的下标
     * @param end   记录末尾位置元素的下标
     */
    public static void quickSort(int[] nums, int start, int end) {
        if (start < end) {
            //把数组中的第0个数当作标准数
            int standard = nums[start];
            //记录需要排序的下标
            int low = start;
            int hight = end;
            //循环找出比标准数大和比标准数小的数
            while (low < hight) {
                //右边数字比标准数大
                while (low < hight && nums[hight] >= standard) {
                    hight--;
                }
                //右边数字比标准数字小,则替换左边的数字
                nums[low] = nums[hight];
                //如果左边数字比标准数小
                while (low < hight && nums[low] <= standard) {
                    low++;
                }
                //如果左边的数比标准数大，则替换掉右边的数
                nums[hight] = nums[low];
            }
            //将标准数赋值给，nums[low]或nums[hight]都可以
            nums[low] = standard;
            //递归调用，对标准数左边的数组进行排序,开始的位置为数组初始的位置，结束位置为下标位置
            quickSort(nums, start, low);
            //递归调用，对标准数右边的数组进行排序，开始的位置为下标加1，结束位置为数组的最后
            quickSort(nums, low + 1, end);
        }
    }
}
