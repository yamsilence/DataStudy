package com.qst.sort;

import java.util.Arrays;

/**
 * @author 刘汉平
 * @date 2019/9/8 16:38
 * 选择排序
 */
public class SelectSort {
    public static void main(String[] args) {
        int[] nums = {6,4,7,3,9,2,1};
        selectSort(nums);
        System.out.println(Arrays.toString(nums));
    }
    public static void selectSort(int[] nums){
        //遍历所有的数
        for (int i=0;i<nums.length;i++){
            //记录最小数的下标，一开始假设第一个数为最小数
            int minIndex = i;
            //遍历最小数后面的数和最小数进行比较
            for (int j=i+1;j<nums.length;j++){
                //如果最小数比这个数大，那么把这个数的下标赋给minIndex
                if (nums[minIndex]>nums[j]){
                    minIndex = j;
                }
            }
            //最后如果i和minIndex的值不相等，则说明这个数组中有比你一开始记录的最小值还要小的数
            //那么交换这两个数
            if(i!=minIndex){
                int temp = nums[i];
                nums[i] = nums[minIndex];
                nums[minIndex] = temp;
            }
        }
    }
}
