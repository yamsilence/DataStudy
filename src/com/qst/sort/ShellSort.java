package com.qst.sort;

import java.util.Arrays;

/**
 * @author 刘汉平
 * @date 2019/9/8 14:51
 * 希尔排序——不会
 */
public class ShellSort {
    public static void main(String[] args) {
        int[] nums = {3,5,4,2,1,7,0,9};
        shellSort(nums);
        System.out.println(Arrays.toString(nums));

    }
    public static void shellSort(int[] nums){
        //遍历所有的布长
        for (int d=nums.length/2;d>0;d/=2){
            //遍历所有元素
            for (int i=d;i<nums.length;i++){
                //遍历本组中所有的元素
                for (int j=i-d;j>=0;j-=d){
                    //如果当前元素大于加上步长后的那个元素
                    if (nums[j]>nums[j+d]){
                        int temp = nums[j];
                        nums[j] = nums[j+d];
                        nums[j+d] = temp;
                    }
                }
            }
        }
    }
}
