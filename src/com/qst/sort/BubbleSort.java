package com.qst.sort;

import java.util.Arrays;

/**
 * @author 刘汉平
 * @date 2019/9/7 17:18
 * 冒泡排序
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 9, 7, 0};
        System.out.println(Arrays.toString(nums));
        bubbleSort(nums);
        System.out.println(Arrays.toString(nums));
    }

    public static void bubbleSort(int[] nums) {
        //控制比较多少轮
        for (int i = 0; i < nums.length - 1; i++) {
            //控制每一轮的比较次数，第一轮需要比较nums.length-1次，因为不用和自己进行比较
            //第二轮需要比较nums.length-1-i次，因为经过第一次的比较，最后一个元素已经确i定是最大，不需再次比较
            for (int j = 0; j < nums.length - 1 -i ; j++) {
                if (nums[j] > nums[j + 1]) {
                    int temp;
                    temp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = temp;
                }
            }
        }
    }
}
