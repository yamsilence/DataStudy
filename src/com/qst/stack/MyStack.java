package com.qst.stack;

/**
 * @author 刘汉平
 * @date 2019/9/4 20:38
 * 使用数组实现一个栈
 */
public class MyStack {
    //创建一个初始数组，初始长度为0
    int[] elements;

    public MyStack() {
        elements = new int[0];
    }

    //压入元素
    public void push(int dst) {
        //创建一个新数组，长度为初始数组的长度加1
        int[] newArray = new int[elements.length + 1];
        //遍历旧数组，将旧数组的元素添加到新数组中
        for (int i = 0; i < elements.length; i++) {
            newArray[i] = elements[i];
        }
        //将传入的元素添加到新数组中
        newArray[elements.length] = dst;
        //新数组替换旧数组
        elements = newArray;
    }
    //取出栈顶元素,
    public int pop(){
        //判断栈是否为空
        if (elements.length==0){
            throw new RuntimeException("Stack is empty");
        }
        //取出栈顶元素
        int element = elements[elements.length-1];
        //创建一个新数组，长度为初始数组长度减1
        int[] newArray = new int[elements.length-1];
        //将元素复制到新数组中
        for (int i=0;i<newArray.length;i++){
            newArray[i] = elements[i];
        }
        //旧数组替换新数组
        elements = newArray;
        //返回栈顶元素
        return element;
    }
    //查看栈顶元素
    public int peek(){
        //判断栈是否为空
        if (elements.length==0){
            throw new RuntimeException("Stack is empty");
        }
        int element = elements[elements.length-1];
        return element;
    }
    //判断栈是否为空
    public boolean isEmpty(){
       return elements.length == 0;
    }
}
