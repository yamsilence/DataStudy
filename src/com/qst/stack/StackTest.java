package com.qst.stack;

/**
 * @author 刘汉平
 * @date 2019/9/5 11:33
 */
public class StackTest {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        //判断一开始栈是不是为空
        System.out.println(myStack.isEmpty());
        //往栈里压入元素
        myStack.push(7);
        myStack.push(8);
        myStack.push(9);
        //查看栈顶元素
        System.out.println(myStack.peek());
        //取出栈顶元素
        myStack.pop();
        //中间插入元素会影响，出栈
        //对于栈这个数据结构来说，规定删除和插入的操作都在线性表的一端，所以中间有元素插入会影响
        myStack.push(6);
        myStack.pop();
        System.out.println(myStack.pop());
       //查看栈顶元素
        //System.out.println(myStack.peek());
    }
}
