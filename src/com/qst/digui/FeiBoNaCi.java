package com.qst.digui;

/**
 * @author 刘汉平
 * @date 2019/9/7 16:12
 * 斐波那契而数列
 * 1 1 2 3 5 8 13 21
 * 除了第一项和第二项之外，其他每项都为前两项的和
 */
public class FeiBoNaCi {
    public static int FeiBoNaQie(int i){
        if (i==1 || i==2){
            return 1;
        }else {
            return FeiBoNaQie(i-1)+FeiBoNaQie(i-2);
        }

    }

    public static void main(String[] args) {
        int i = FeiBoNaQie(8);
        System.out.println(i);
    }
}
