package com.qst.digui;

/**
 * @author 刘汉平
 * @date 2019/9/7 14:49
 * 汉诺塔问题，是一个典型的递归问题
 * 不论有多少个盘子，都只看成两个盘子，最上面的一堆盘子和最下面的那个盘子
 * 只需将上面的盘子移动到中间那个盘子，然后最下面的一个移动到第三个盘子，中间的盘子再移动到第三个即可
 */
public class HanNuoTa {
    public static void main(String[] args) {
          hanoi(2,'A','B','C');
    }

    /**
     * @param n 共有多少个盘子
     * @param from 开始的柱子
     * @param in 中间的柱子
     * @param to 目标柱子
     */
    public static void  hanoi(int n,char from,char in,char to){
        if (n==1){
            System.out.println("第一个盘子从"+from+"移动到"+to);
        }else {
            //移动上面所有的盘子到中间
            hanoi(n-1,from,to,in);
            System.out.println("第"+n+"个盘子从"+from+"移动到"+to);
            hanoi(n-1,in,from,to);
        }
    }
}
