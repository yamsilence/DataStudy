package com.qst.array;

/**
 * @author 刘汉平
 * @date 2019/9/6 9:52
 * 给定一个排序数组，你需要在原地删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
 * 不要使用额外的数组空间，你必须在原地修改输入数组并在使用 O(1) 额外空间的条件下完成。
 */
public class Solution {
    public static int removeDuplicates(int[] nums) {
        //如果数组的长度为空或者长度为0，则返回0
        if (nums == null || nums.length == 0) {
            return 0;
            //如果数组的长度为1，则返回1
        } else if (nums.length == 1) {
            return 1;
        } else {
            //定义一个临时变量，这个变量就是数组的第一个元素
            int temp = nums[0];
            //可以看成是一个计数器，记录非重复数字的个数
            int len = 1;
            //循环遍历数组，从数组的第二个元素开始遍历
            for (int j = 1; j < nums.length; j++) {
                //如果数组的第二个元素和第一个元素相等，则跳出本次循环
                if (temp == nums[j]) {
                    continue;
                } else {
                    //如果这两个元素不相等，则将这个不相等的元素赋给，临时变量temp，这个临时变量可以看成是一个指针
                    temp = nums[j];
                    //nums[len] = nums[j];// 如果只是想等到长度的话,这段代码是可以不用写的
                    //然后计数器加1
                    len++;
                }
            }
            return len;
        }


    }

    //方法二
    public static int removeDuplicates2(int[] array) {
        //如果这个数组的长度为0，则返回0
        if (array.length==0){
            return 0;
        }
        //定义一个变量，为0，ayyay[0]代表数组的第一个元素，i相当于一个计数器的作用，记录了不重复元素的个数，但是
        //最后不要忘了加1，因为是从0开始没有算第一个元素
        int i = 0;
        //j从1开始，如果说不相等，则i加1
        for (int j=1;j<array.length;j++){
            if (array[j]!=array[i]){
                i++;
                //将不相等的哪个数，赋值给array[i]，
                array[i]=array[j];
            }
        }
       return i+1;
    }


    public static void main(String[] args) {
        int[] nums = new int[]{1, 1, 1, 1, 2, 2, 3, 4, 5};
       // int i = Solution.removeDuplicates(nums);
        int i = Solution.removeDuplicates2(nums);
        System.out.println(i);

    }



}



