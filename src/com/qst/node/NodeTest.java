package com.qst.node;

/**
 * @author 刘汉平
 * @date 2019/9/5 15:36
 */
public class NodeTest {
    public static void main(String[] args) {
        Node node = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        node.append(node2).append(node3).append(node4);
        //node.show();
        // node.next.remove();
        //node.show();
        node.next.after(node5);
        node.show();
    }
}
