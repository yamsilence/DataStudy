package com.qst.node;

/**
 * @author 刘汉平
 * @date 2019/9/5 19:52
 */
public class DoubleNodeTest {
    public static void main(String[] args) {
        DoubleNode dn1 = new DoubleNode(1);
        DoubleNode dn2 = new DoubleNode(2);
        DoubleNode dn3 = new DoubleNode(3);
        DoubleNode dn4 = new DoubleNode(4);
        dn1.after(dn2);
        System.out.println(dn1.pre().getData());//2
        System.out.println(dn1.getData());//1
        System.out.println(dn1.next().getData());//2
        System.out.println("==================");
        dn2.after(dn3);
        System.out.println(dn2.pre().getData());//1
        System.out.println(dn2.getData());//2
        System.out.println(dn2.next().getData());//3
    }
}
