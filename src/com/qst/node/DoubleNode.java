package com.qst.node;

/**
 * @author 刘汉平
 * @date 2019/9/5 19:34
 * 双向循环链表
 */
public class DoubleNode {
    //当前节点的上一个节点
    DoubleNode pre = this;
    //当前节点的下一个节点
    DoubleNode next = this;
    //当前节点的值
    int data;
    public DoubleNode(int data) {
        this.data = data;
    }

    //插入节点
    public void after(DoubleNode node){
        //先获取当前节点的下一个节点
        DoubleNode nextNext = next;
        //当前节点的下一个节点为新节点
        this.next = node;
        //新节点的上一个节点为当前节点
        node.pre = this;
        //新节点的下一个节点，为当前节点的下一个节点
        node.next = nextNext;
        //当前节点的下一个节点，的上一个节点为新节点
        nextNext.pre = node;
    }
    //获取上一个节点
    public DoubleNode pre(){
        return this.pre;
    }
    //获取下一个节点
    public DoubleNode next(){
        return this.next;
    }
    //获取节点的值
    public int getData(){
        return this.data;
    }
}
