package com.qst.node;

/**
 * @author 刘汉平
 * @date 2019/9/5 15:13
 * 这是一个节点
 */
public class Node {
    //表示当前节点的值
    int data;
    //表示当前节点的下一个节点
    Node next;

    public Node(int data) {
        this.data = data;
    }

    //为节点追加节点
    public Node append(Node node) {
        //当前节点
        Node currentNode = this;
        //循环向后找当前节点的下一个节点
        while (true) {
            Node nextNode = currentNode.next;
            //如果当前节点的下一个节点为空，则说明是最后一个节点了，跳出循环
            if (nextNode == null) {
                break;
            }
            //如果当前节点的下一个节点不为空，则将下一个节点赋给当前节点，继续向后循环，直到当前节点的下一个节点为空
            currentNode = nextNode;
        }
        //将你传入的这个节点，赋给当前节点的下一个节点
        currentNode.next = node;
        return this;
    }
    //删除节点
    public void remove(){
        //当前节点的下下个节点等于当前节点的下一个节点，这样就完成了节点的删除
        Node nextNode = next.next;
        this.next = nextNode;
    }
    //节点的插入，由于是单链表，所以只能在节点后面插入节点
    public void after(Node node){
        //拿到当前节点的下一个节点
        Node nextNode = next;
        //当前节点的下一个节点为，你新添加的节点
        this.next = node;
        //新节点的下一个节点等于nextNode
        node.next = nextNode;
    }
    //节点的展示
    public void show(){
        Node currenrNode = this;
        while (true){
            //打印当前节点
            System.out.print(currenrNode.data+" ");
            //将当前节点的下一个节点赋给当前节点
            currenrNode = currenrNode.next;
            if (currenrNode == null){
                break;
            }
        }
        System.out.println();
    }
    //当前节点的下一个节点
    public Node next() {
        return this.next;
    }

    //当前节点的值
    public int getData() {
        return this.data;
    }

    //当前节点是否为最后一个节点
    public boolean isLast() {
        return this.next == null;
    }
}
