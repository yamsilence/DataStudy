package com.qst.node;

/**
 * @author 刘汉平
 * @date 2019/9/5 16:54
 * 循环链表
 */
public class LoopNode {
    //表示当前节点的值
    int data;
    //表示当前节点的下一个节点
    LoopNode next = this;

    public LoopNode(int data) {
        this.data = data;
    }
    //节点插入
    public void after(LoopNode node){
        //拿到当前节点的下一个节点
        LoopNode nextNode = next;
        //当前节点的下一个节点为，你新添加的节点
        this.next = node;
        //新节点的下一个节点等于nextNode
        node.next = nextNode;
    }
    //删除节点
    public void remove(){
        //当前节点的下下个节点等于当前节点的下一个节点，这样就完成了节点的删除
        LoopNode nextNode = next.next;
        this.next = nextNode;
    }
    //节点的展示
    public void show(){
        LoopNode currenrNode = this;
        while (true){
            //打印当前节点
            System.out.print(currenrNode.data+" ");
            //将当前节点的下一个节点赋给当前节点
            currenrNode = currenrNode.next;
            if (currenrNode == null){
                break;
            }
        }
        System.out.println();
    }
    //当前节点的下一个节点
    public LoopNode next() {
        return this.next;
    }

    //当前节点的值
    public int getData() {
        return this.data;
    }

    //当前节点是否为最后一个节点
    public boolean isLast() {
        return this.next == null;
    }
}
