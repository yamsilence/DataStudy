package com.qst.node;

/**
 * @author 刘汉平
 * @date 2019/9/5 16:57
 */
public class LoopNodeTest {
    public static void main(String[] args) {
        LoopNode lp1 = new LoopNode(1);
        LoopNode lp2 = new LoopNode(2);
        LoopNode lp3 = new LoopNode(3);
        LoopNode lp4 = new LoopNode(4);
        LoopNode lp5 = new LoopNode(5);
        lp1.after(lp2);
        lp2.after(lp3);
        lp3.after(lp4);
        System.out.println(lp1.next.getData());//2
        System.out.println(lp2.next.getData());//3
        System.out.println(lp3.next.getData());//4
        System.out.println(lp4.next.getData());//1
    }
}
