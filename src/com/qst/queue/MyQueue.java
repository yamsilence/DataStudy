package com.qst.queue;

/**
 * @author 刘汉平
 * @date 2019/9/5 14:00
 */
public class MyQueue {
    int[] elements;

    public MyQueue() {
        elements = new int[0];
    }

    //进队
    public void add(int dest) {
        //创建一个新的数组，长度为旧数组的长度加1
        int[] newArray = new int[elements.length + 1];
        //遍历旧的数组，将数据复制进去
        for (int i = 0; i < elements.length; i++) {
            newArray[i] = elements[i];
        }
        //将传入的数据添加到新的数组中
        newArray[elements.length] = dest;
        //替换数组
        elements = newArray;
    }

    //出队
    public int pop() {
        //将数组的第一个元素返回出去，也就是对头
        int element = elements[0];
        //创建一个新的数组，长度为原数组长度减1
        int[] newArray = new int[elements.length - 1];
        //将返回出去元素的数组，复制到新的数组中，因为一个元素已经返回出去，所以是i+1=i
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = elements[i + 1];
        }
        elements = newArray;
        return element;
    }

    //判断队是否为空
    public boolean isEmpty() {
        return elements.length == 0;
    }
}
