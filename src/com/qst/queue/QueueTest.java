package com.qst.queue;

/**
 * @author 刘汉平
 * @date 2019/9/5 14:14
 */
public class QueueTest {
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.add(4);
        myQueue.add(5);
        myQueue.add(6);
        myQueue.add(7);
        myQueue.add(8);
        myQueue.add(9);
        myQueue.pop();
        //中间有元素进队列不影响出队列
        //对于队列这个线性结构来说，规定插入在线性表的一端，删除在线性表的另一端，所以中间有元素插入，并不影响
        myQueue.add(10);
        myQueue.pop();
        myQueue.pop();
        System.out.println(myQueue.pop());
    }
}
